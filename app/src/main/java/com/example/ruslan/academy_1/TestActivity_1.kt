package com.example.ruslan.academy_1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import com.example.ruslan.academy_1.adapter.MessagesAdapter
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.DefaultItemAnimator
import java.util.List
import android.widget.Toast
import com.example.ruslan.academy_1.network.ApiInterface
import com.example.ruslan.academy_1.network.ApiClient
import kotlinx.android.synthetic.main.activity_test_1.*
import okhttp3.Call
import retrofit2.Callback
import retrofit2.Response


class TestActivity_1 : AppCompatActivity(), MessagesAdapter.MessageAdapterListener {


    private var messages = ArrayList<Message>()
    private var mAdapter: MessagesAdapter? = null
    //private var swipeRefreshLayout: SwipeRefreshLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_1)


        mAdapter = MessagesAdapter(this,messages as List<Message>,this)


        recycler_view!!.layoutManager = LinearLayoutManager(applicationContext)
        //recyclerView!!.itemAnimator = DefaultItemAnimator()
        //recyclerView!!.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        recycler_view.adapter = mAdapter


        // show loader and fetch messages
        swipe_refresh_layout!!.post(
                Runnable { getInbox() }
        )
    }


    /**
     * Fetches mail messages by making HTTP request
     * url: https://api.androidhive.info/json/inbox.json
     */
    private fun getInbox() {
        swipe_refresh_layout!!.setRefreshing(true)

        val apiService = ApiClient.getClient()!!.create(ApiInterface::class.java)

        val call = apiService.inbox
        call.enqueue(object : Callback<List<Message>> {
            override fun onResponse(call: retrofit2.Call<List<Message>>, response: Response<List<Message>>) {
                // clear the inbox
                messages.clear()

                // add all the messages
                // messages.addAll(response.body());

                // TODO - avoid looping
                // the loop was performed to add colors to each message
                for (message in response.body()) {
                    // generate a random color
                    //message.setColor(getRandomMaterialColor("400"))
                    messages.add(message)
                }

                mAdapter!!.notifyDataSetChanged()
                swipe_refresh_layout!!.setRefreshing(false)
            }

            override fun onFailure(call: retrofit2.Call<List<Message>>, t: Throwable) {
                Toast.makeText(applicationContext, "Unable to fetch json: " + t.message, Toast.LENGTH_LONG).show()
                swipe_refresh_layout!!.setRefreshing(false)
            }
        })
    }


    override fun onIconClicked(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onIconImportantClicked(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMessageRowClicked(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRowLongClicked(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}
