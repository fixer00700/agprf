package com.example.ruslan.academy_1

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import com.ice.restring.Restring
import kotlinx.android.synthetic.main.fragment_1.*


class MyCustomApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: MyCustomApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }

        fun GetStringFromStringsJSON(textView: TextView?,context: Context?): CharSequence? {

            val text = context?.resources?.openRawResource(R.raw.test)?.bufferedReader().use { it?.readText() }

            val gson = Gson()

            val type = object : TypeToken<LinkedTreeMap<String, String>>() {}.type

            val myMap = gson.fromJson<LinkedTreeMap<String, String>>(text, type)

            var ViewTextId = context?.getResources()?.getResourceEntryName(textView!!.id);

            return myMap[ViewTextId].toString()
        }

        fun GetVersionFromJSON(context: Context?): CharSequence? {

            val text = context?.resources?.openRawResource(R.raw.test)?.bufferedReader().use { it?.readText() }

            val gson = Gson()

            val type = object : TypeToken<LinkedTreeMap<String, String>>() {}.type

            val myMap = gson.fromJson<LinkedTreeMap<String, String>>(text, type)

            return myMap["ver"].toString()
        }
    }



    override fun onCreate() {
        super.onCreate()
        // initialize for any

        // Use ApplicationContext.
        // example: SharedPreferences etc...
        val context: Context = MyCustomApplication.applicationContext()
    }
}


