package com.example.ruslan.academy_1.adapter

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;


import com.example.ruslan.academy_1.Message;
import java.nio.file.Files.delete
import com.example.ruslan.academy_1.adapter.MessagesAdapter.MyViewHolder
import android.R.attr.thumbnail
import android.os.Parcel
import android.os.Parcelable
import com.example.ruslan.academy_1.R






class MessagesAdapter() : RecyclerView.Adapter<MyViewHolder>(), Parcelable {

    private var mContext: Context? = null
    private var messages: List<Message>? = null
    private var listener: MessageAdapterListener? = null
    private var selectedItems: SparseBooleanArray? = null

    // array used to perform multiple animation at once
    private var animationItemsIndex: SparseBooleanArray? = null
    private var reverseAllAnimations = false

    // index is used to animate only the selected row
    // dirty fix, find a better solution
    private var currentSelectedIndex = -1

    constructor(parcel: Parcel) : this() {
        selectedItems = parcel.readSparseBooleanArray()
        animationItemsIndex = parcel.readSparseBooleanArray()
        reverseAllAnimations = parcel.readByte() != 0.toByte()
        currentSelectedIndex = parcel.readInt()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnLongClickListener {

        var from: TextView
        var subject: TextView
        var iconText: TextView
        var messageContainer: LinearLayout
        var iconContainer: RelativeLayout

        init {
            from = view.findViewById(R.id.from) as TextView
            subject = view.findViewById(R.id.txt_primary) as TextView
            iconText = view.findViewById(R.id.icon_text) as TextView
            messageContainer = view.findViewById(R.id.message_container) as LinearLayout
            iconContainer = view.findViewById(R.id.icon_container) as RelativeLayout
            view.setOnLongClickListener(this)
        }

        override fun onLongClick(view: View): Boolean {
            listener?.onRowLongClicked(adapterPosition)
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
            return true
        }
    }

    constructor (mContext: Context, messages: List<Message>, listener: MessageAdapterListener) : this() {
        this.mContext = mContext
        this.messages = messages
        this.listener = listener
        selectedItems = SparseBooleanArray()
        animationItemsIndex = SparseBooleanArray()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.message_list_row, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val message = messages!!.get(position)

        // displaying text view data
        holder.from.text = message.getFrom()
        holder.subject.text = message.getSubject()


        // displaying the first letter of From in icon text
        holder.iconText.text = message.getFrom()!!.substring(0, 1)

        // change the row state to activated
        //holder.itemView.isActivated = selectedItems!!.get(position, false)


        // apply click events
        applyClickEvents(holder, position)
    }

    private fun applyClickEvents(holder: MyViewHolder, position: Int) {
        holder.iconContainer.setOnClickListener { listener!!.onIconClicked(position) }

        holder.messageContainer.setOnClickListener { listener!!.onMessageRowClicked(position) }

        holder.messageContainer.setOnLongClickListener { view ->
            listener!!.onRowLongClicked(position)
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
            true
        }
    }





    override fun getItemId(position: Int): Long {
        return messages!!.get(position).getId().toLong()
    }




    override fun getItemCount(): Int {
        return messages!!.size
    }

    fun toggleSelection(pos: Int) {
        currentSelectedIndex = pos
        if (selectedItems!!.get(pos, false)) {
            selectedItems!!.delete(pos)
            animationItemsIndex!!.delete(pos)
        } else {
            selectedItems!!.put(pos, true)
            animationItemsIndex!!.put(pos, true)
        }
        notifyItemChanged(pos)
    }

    fun clearSelections() {
        reverseAllAnimations = true
        selectedItems!!.clear()
        notifyDataSetChanged()
    }

    fun getSelectedItemCount(): Int {
        return selectedItems!!.size()
    }

    fun getSelectedItems(): kotlin.collections.List<Int> {
        val items = ArrayList<Int>(selectedItems!!.size())
        for (i in 0 until selectedItems!!.size()) {
            items.add(selectedItems!!.keyAt(i))
        }
        return items.toList()
    }

    fun removeData(position: Int) {
        messages!!.remove(position)
        resetCurrentIndex()
    }

    private fun resetCurrentIndex() {
        currentSelectedIndex = -1
    }

    interface MessageAdapterListener {
        fun onIconClicked(position: Int)

        fun onIconImportantClicked(position: Int)

        fun onMessageRowClicked(position: Int)

        fun onRowLongClicked(position: Int)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeSparseBooleanArray(selectedItems)
        parcel.writeSparseBooleanArray(animationItemsIndex)
        parcel.writeByte(if (reverseAllAnimations) 1 else 0)
        parcel.writeInt(currentSelectedIndex)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MessagesAdapter> {
        override fun createFromParcel(parcel: Parcel): MessagesAdapter {
            return MessagesAdapter(parcel)
        }

        override fun newArray(size: Int): Array<MessagesAdapter?> {
            return arrayOfNulls(size)
        }
    }
}