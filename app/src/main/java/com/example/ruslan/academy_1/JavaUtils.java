package com.example.ruslan.academy_1;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

public class JavaUtils {

    public JavaUtils(){

        Gson gson = new Gson();

        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> myMap = gson.fromJson("{'k1':'apple','k2':'orange'}", type);

    }

    public static Map<String, String> GetData(){

        Gson gson = new Gson();

        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> myMap = gson.fromJson("{'k1':'apple','k2':'orange'}", type);

        return myMap;
    }
}
