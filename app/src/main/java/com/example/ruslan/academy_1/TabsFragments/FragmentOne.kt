package com.example.ruslan.academy_1

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.ruslan.academy_1.adapter.MessagesAdapter
import com.example.ruslan.academy_1.network.ApiClient
import com.example.ruslan.academy_1.network.ApiInterface
import kotlinx.android.synthetic.main.activity_test_1.*
import kotlinx.android.synthetic.main.fragment_one.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.List

/**
 * Created by Chike on 12/11/2017.
 */
class FragmentOne : Fragment(), MessagesAdapter.MessageAdapterListener {

    private var messages = ArrayList<Message>()
    private var mAdapter: MessagesAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater!!.inflate(R.layout.fragment_one, container, false)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mAdapter = MessagesAdapter(this.context!!,messages as List<Message>,this)


        recycler_view_1!!.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        //recyclerView!!.itemAnimator = DefaultItemAnimator()
        //recyclerView!!.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        recycler_view_1.adapter = mAdapter


        // show loader and fetch messages
        swipe_refresh_layout_1!!.post(
                Runnable { getInbox() }
        )

    }



    companion object {
        fun newInstance(): FragmentOne = FragmentOne()
    }

    /**
     * Fetches mail messages by making HTTP request
     * url: https://api.androidhive.info/json/inbox.json
     */
    private fun getInbox() {
        swipe_refresh_layout_1!!.setRefreshing(true)

        val apiService = ApiClient.getClient()!!.create(ApiInterface::class.java)

        val call = apiService.inbox
        call.enqueue(object : Callback<List<Message>> {
            override fun onResponse(call: retrofit2.Call<List<Message>>, response: Response<List<Message>>) {
                // clear the inbox
                messages.clear()

                // add all the messages
                // messages.addAll(response.body());

                // TODO - avoid looping
                // the loop was performed to add colors to each message
                for (message in response.body()) {
                    // generate a random color
                    //message.setColor(getRandomMaterialColor("400"))
                    messages.add(message)
                }

                mAdapter!!.notifyDataSetChanged()
                swipe_refresh_layout_1!!.setRefreshing(false)
            }

            override fun onFailure(call: Call<List<Message>>, t: Throwable) {
                Toast.makeText(activity, "Unable to fetch json: " + t.message, Toast.LENGTH_LONG).show()
                swipe_refresh_layout_1!!.setRefreshing(false)
            }
        })
    }

    override fun onIconClicked(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onIconImportantClicked(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMessageRowClicked(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRowLongClicked(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}