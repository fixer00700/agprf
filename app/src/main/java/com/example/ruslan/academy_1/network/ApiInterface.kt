package com.example.ruslan.academy_1.network

import java.util.List;

import com.example.ruslan.academy_1.Message;
import retrofit2.Call;
import retrofit2.http.GET;

interface ApiInterface {

    @get:GET("inbox.json")
    val inbox: Call<List<Message>>

}